package org.apache.synapse.core.axis2;

/**
 * Class to keep the response's state
 */
public class ResponseState {
    private boolean isRespondDone = false;

    /**
     * Return the response is done or not.
     *
     * @return isRespondDone {boolean}
     */
    public boolean isRespondDone() {
        return isRespondDone;
    }

    /**
     * Set the response to done or not.
     */
    public void setRespondDone() {
        isRespondDone = true;
    }
}
